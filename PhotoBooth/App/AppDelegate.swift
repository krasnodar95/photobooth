//
//  AppDelegate.swift
//  PhotoBooth
//
//  Created by Руслан on 15.03.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        self.window = makeWindow()
        self.window?.makeKeyAndVisible()

        return true
    }

    func makeWindow() -> UIWindow {
        let screenFactory = ScreenFactory()
        let navigationController = UINavigationController()
        let router = Router(navigationController: navigationController)
        let rootViewController = screenFactory.makeMenu(router: router)

        navigationController.setViewControllers([rootViewController], animated: false)

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = navigationController

        return window
    }
}
