//
//  StatusView.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

final class StatusView: UIView {
    struct Model: Equatable {
        let title: String
        let image: UIImage
    }

    private let titleLabel: UILabel!
    private let imageView: UIImageView!
    private let stackView: UIStackView!

    init() {
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = .boldSystemFont(ofSize: 20.0)
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center

        imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit

        stackView = UIStackView(arrangedSubviews: [imageView, titleLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 30.0

        super.init(frame: .zero)

        self.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.rightAnchor.constraint(equalTo: self.rightAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            stackView.leftAnchor.constraint(equalTo: self.leftAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 80.0),
            imageView.heightAnchor.constraint(equalToConstant: 80.0)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(with model: Model) {
        titleLabel.text = model.title
        imageView.image = model.image
    }
}
