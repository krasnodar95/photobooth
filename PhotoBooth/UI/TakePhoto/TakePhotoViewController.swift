//
//  TakePhotoViewController.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit
import MobileCoreServices

final class TakePhotoViewController: UIViewController {
    private let viewModel: TakePhotoViewModel

    private var statusView: StatusView!
    private var photoEditView: PhotoEditView!
    private var saveButton: UIBarButtonItem!

    init(viewModel: TakePhotoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = UIView()
        view.backgroundColor = .white

        statusView = StatusView()
        statusView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(statusView)

        photoEditView = PhotoEditView(
            makePhotoTapped: { [unowned self] in
                self.viewModel.makePhotoSelected()
            },
            setNameTapped: { [unowned self] in
                self.viewModel.setNameSelected()
            }
        )
        photoEditView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(photoEditView)

        saveButton = .init(barButtonSystemItem: .save, target: self, action: #selector(saveTapped))

        NSLayoutConstraint.activate([
            statusView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            statusView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 80.0),
            statusView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -80.0),
            photoEditView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            photoEditView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 80.0),
            photoEditView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -80.0)
        ])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.screenStateSignal.subscribe { [weak self] state in
            guard let self = self else { return }

            switch state {
            case let .warning(model):
                self.navigationItem.rightBarButtonItem = nil
                self.statusView.isHidden = false
                self.photoEditView.isHidden = true
                self.statusView.update(with: model)
            case let .normal(model, isSaveEnabled):
                self.navigationItem.rightBarButtonItem = self.saveButton
                self.saveButton.isEnabled = isSaveEnabled
                self.statusView.isHidden = true
                self.photoEditView.isHidden = false
                self.photoEditView.update(with: model)
            }
        }

        viewModel.pickImageSignal.subscribe { [weak self] in
            self?.openImagePicker()
        }

        viewModel.viewDidLoad()
    }

    private func openImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.mediaTypes = [String(kUTTypeImage)]
        imagePicker.delegate = self

        present(imagePicker, animated: true, completion: nil)
    }

    @objc private func saveTapped() {
        viewModel.saveSelected()
    }
}

extension TakePhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        guard let image = info[.originalImage] as? UIImage else {
            return
        }

        viewModel.apply(image: image)

        dismiss(animated: true, completion: nil)
    }
}
