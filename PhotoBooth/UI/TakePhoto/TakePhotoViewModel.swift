//
//  TakePhotoViewModel.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

final class TakePhotoViewModel {
    enum ScreenState {
        case normal(PhotoEditView.Model, isSaveEnabled: Bool)
        case warning(StatusView.Model)
    }

    let screenStateSignal = Signal<ScreenState>()
    let pickImageSignal = Signal<Void>()

    private let router: RouterProtocol
    private let screenFactory: ScreenFactoryProtocol
    private let cameraPermissionsService: CameraPermissionsServiceProtocol
    private let photoRepository: PhotoRepositoryProtocol

    private var image: UIImage?
    private var name: String?

    init(
        router: RouterProtocol,
        screenFactory: ScreenFactoryProtocol,
        cameraPermissionsService: CameraPermissionsServiceProtocol,
        photoRepository: PhotoRepositoryProtocol
    ) {
        self.router = router
        self.screenFactory = screenFactory
        self.cameraPermissionsService = cameraPermissionsService
        self.photoRepository = photoRepository
    }

    func viewDidLoad() {
        screenStateSignal.send(.warning(.init(title: "Trying to get camera access", image: .init(imageLiteralResourceName: "hourglass"))))

        let cameraStatus = cameraPermissionsService.checkStatus()
        switch cameraStatus {
        case .needRequestAccess:
            cameraPermissionsService.requestGrants { isGranted in
                if isGranted {
                    self.showNormalState()
                } else {
                    self.showAccessDeniedState()
                }
            }
        case .accessGranted:
            showNormalState()
        case .accessDenied:
            showAccessDeniedState()
        case .noTechnicalPossibility:
            showNoTechState()
        }
    }

    func makePhotoSelected() {
        openImagePicker()
    }

    func apply(image: UIImage) {
        self.image = image
        showNormalState()
    }

    func setNameSelected() {
        openNameEditor()
    }

    func saveSelected() {
        guard let image = image, let name = name else {
            return
        }

        photoRepository.savePhoto(image: image, name: name) { [weak self] result in
            switch result {
            case .success:
                self?.router.jumpBackToRoot()
            case let .failure(error):
                print("Problem with persistance: \(error)")
            }
        }
    }

    private func showNormalState() {
        let isSaveEnabled = image != nil && name != nil
        screenStateSignal.send(.normal(.init(image: image, name: name), isSaveEnabled: isSaveEnabled))
    }

    private func showAccessDeniedState() {
        screenStateSignal.send(.warning(.init(title: "Camera permission denied. Please give permissions in system settings ⚙️", image: .init(imageLiteralResourceName: "attention-sign"))))
    }

    private func showNoTechState() {
        screenStateSignal.send(.warning(.init(title: "There is no camera on device", image: .init(imageLiteralResourceName: "no-photo"))))
    }

    private func openImagePicker() {
        pickImageSignal.send()
    }

    private func openNameEditor() {
        router.showModal(screenFactory.makePrompt(currentText: name) { [weak self] name in
            self?.name = name
            self?.showNormalState()
        })
    }
}
