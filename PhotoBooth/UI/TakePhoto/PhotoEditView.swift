//
//  PhotoEditView.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

final class PhotoEditView: UIView {
    struct Model {
        let image: UIImage?
        let name: String?
    }

    private var imageView: UIImageView!
    private var makePhotoButton: UIButton!
    private var nameLabel: UILabel!
    private var setNameButton: UIButton!
    private var stackView: UIStackView!

    init(
        makePhotoTapped: @escaping () -> Void,
        setNameTapped: @escaping () -> Void
    ) {
        super.init(frame: .null)

        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true

        makePhotoButton = self.makeButton(title: "Make photo", tapAction: {
            makePhotoTapped()
        })

        nameLabel = UILabel()
        nameLabel.textColor = .black
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.numberOfLines = 0
        nameLabel.translatesAutoresizingMaskIntoConstraints = false

        setNameButton = self.makeButton(title: "Set name", tapAction: {
            setNameTapped()
        })

        stackView = UIStackView(arrangedSubviews: [imageView, makePhotoButton, nameLabel, setNameButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 30.0

        addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.rightAnchor.constraint(equalTo: self.rightAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            stackView.leftAnchor.constraint(equalTo: self.leftAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 200.0),
            imageView.heightAnchor.constraint(equalToConstant: 200.0)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(with model: Model) {
        imageView.image = model.image
        nameLabel.text = model.name
    }

    private func makeButton(title: String, tapAction: @escaping () -> Void) -> UIButton{
        let button = UIButton(type: .system, primaryAction: .init { _ in
            tapAction()
        })
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(title, for: .normal)

        return button
    }
}
