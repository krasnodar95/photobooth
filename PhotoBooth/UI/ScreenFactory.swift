//
//  ScreenFactory.swift
//  PhotoBooth
//
//  Created by Руслан on 15.03.2021.
//

import UIKit

public protocol ScreenFactoryProtocol {
    func makeMenu(router: RouterProtocol) -> UIViewController
    func makeTakePhoto(router: RouterProtocol) -> UIViewController
    func makePrompt(currentText: String?, completion: @escaping (String?) -> Void) -> UIViewController
    func makePhotoList(router: RouterProtocol) -> UIViewController
    func makePhotoDetails(photoUUID: UUID) -> UIViewController
}

final class ScreenFactory: ScreenFactoryProtocol {
    private let cameraPermissionsService = CameraPermissionsService()
    private let photoRepository = PhotoRepository(persistentService: .init())

    func makeMenu(router: RouterProtocol) -> UIViewController {
        let viewModel = MenuViewModel(
            router: router,
            screenFactory: self
        )
        return MenuViewController(viewModel: viewModel)
    }

    func makeTakePhoto(router: RouterProtocol) -> UIViewController {
        let viewModel = TakePhotoViewModel(
            router: router,
            screenFactory: self,
            cameraPermissionsService: cameraPermissionsService,
            photoRepository: photoRepository
        )
        return TakePhotoViewController(viewModel: viewModel)
    }

    func makePrompt(currentText: String?, completion: @escaping (String?) -> Void) -> UIViewController {
        let alert = UIAlertController(title: "Please enter name", message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.text = currentText
        }

        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned alert] _ in
            let answer = alert.textFields?.first?.text
            completion(answer)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(submitAction)
        alert.addAction(cancelAction)

        return alert
    }

    func makePhotoList(router: RouterProtocol) -> UIViewController {
        let viewModel = PhotoListViewModel(
            router: router,
            screenFactory: self,
            photoRepository: photoRepository
        )
        return PhotoListViewController(viewModel: viewModel)
    }

    func makePhotoDetails(photoUUID: UUID) -> UIViewController {
        let viewModel = PhotoDetailsViewModel(
            photoRepository: photoRepository,
            photoUUID: photoUUID
        )
        return PhotoDetailsViewController(viewModel: viewModel)
    }
}
