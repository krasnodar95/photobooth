//
//  PhotoDetailsViewController.swift
//  PhotoBooth
//
//  Created by Руслан on 17.03.2021.
//

import UIKit

final class PhotoDetailsViewController: UIViewController {
    private let viewModel: PhotoDetailsViewModel

    private var imageView: UIImageView!
    private var nameLabel: UILabel!
    private var dateLabel: UILabel!
    private var stackView: UIStackView!

    init(viewModel: PhotoDetailsViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = UIView()
        view.backgroundColor = .white

        imageView = .init()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        view.addSubview(imageView)

        nameLabel = .init()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.numberOfLines = 0
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.textColor = .black
        nameLabel.font = .boldSystemFont(ofSize: 30.0)

        dateLabel = .init()
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.textColor = .darkGray
        dateLabel.font = .boldSystemFont(ofSize: 20.0)

        stackView = .init(arrangedSubviews: [nameLabel, dateLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.layer.cornerRadius = 15.0
        stackView.layer.backgroundColor = UIColor.white.withAlphaComponent(0.4).cgColor
        view.addSubview(stackView)

        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            imageView.rightAnchor.constraint(equalTo: view.rightAnchor),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 30.0),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30.0),
            stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30.0)
        ])
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.updateSignal.subscribe { [weak self] (image, name, createdDate) in
            guard let self = self else { return }

            self.imageView.image = image
            self.nameLabel.text = name
            self.dateLabel.text = createdDate
        }

        viewModel.viewDidLoad()
    }
}
