//
//  PhotoDetailsViewModel.swift
//  PhotoBooth
//
//  Created by Руслан on 17.03.2021.
//

import UIKit

final class PhotoDetailsViewModel {
    private let photoRepository: PhotoRepositoryProtocol
    private let photoUUID: UUID

    let updateSignal = Signal<(UIImage, String, String)>()

    init(photoRepository: PhotoRepositoryProtocol, photoUUID: UUID) {
        self.photoRepository = photoRepository
        self.photoUUID = photoUUID
    }

    func viewDidLoad() {
        photoRepository.fetchPhoto(with: photoUUID) { [weak self] result in
            switch result {
            case let .success(model):
                guard let model = model else {
                    print("Model is empty")
                    return
                }

                self?.presentPhoto(with: model)

            case let .failure(error):
                print("Something went wrong: \(error)")
            }
        }
    }

    private func presentPhoto(with model: PhotoModel) {
        updateSignal.send((model.image, model.name, model.createdAt.toString()))
    }
}
