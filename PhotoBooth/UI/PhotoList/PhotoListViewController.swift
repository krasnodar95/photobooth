//
//  PhotoListViewController.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

final class PhotoListViewController: UITableViewController {
    private let viewModel: PhotoListViewModel

    init(viewModel: PhotoListViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = 100.0
        tableView.estimatedRowHeight = 100.0

        tableView.backgroundColor = .white

        viewModel.reloadSignal.subscribe { [weak self] in
            self?.tableView.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.viewWillAppear()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.photoList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
            cell!.imageView?.contentMode = .scaleAspectFill
            cell!.imageView?.clipsToBounds = true
            cell!.accessoryType = .disclosureIndicator
        }

        let model = viewModel.photoList[indexPath.row]

        cell!.update(with: model)

        return cell!
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let item = viewModel.photoList[indexPath.row]
        viewModel.selectedItem(with: item.uuid)
    }
}
