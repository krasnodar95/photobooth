//
//  PhotoListViewModel.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

final class PhotoListViewModel {
    private let router: RouterProtocol
    private let screenFactory: ScreenFactoryProtocol
    private let photoRepository: PhotoRepositoryProtocol

    let reloadSignal = Signal<Void>()

    private(set) var photoList = [UITableViewCell.Model]()

    init(router: RouterProtocol, screenFactory: ScreenFactoryProtocol, photoRepository: PhotoRepositoryProtocol) {
        self.router = router
        self.photoRepository = photoRepository
        self.screenFactory = screenFactory
    }

    func viewWillAppear() {
        photoRepository.fetchPhotoList { [weak self] result in
            switch result {
            case let .success(photoList):
                self?.handle(photoList: photoList)
            case let .failure(error):
                print("Something went wrong \(error)")
            }
        }
    }

    func selectedItem(with photoUUID: UUID) {
        router.showPushed(screenFactory.makePhotoDetails(photoUUID: photoUUID))
    }

    private func handle(photoList: [PhotoModel]) {
        self.photoList = photoList.compactMap { UITableViewCell.Model(with: $0) }
        self.reloadSignal.send()
    }
}
