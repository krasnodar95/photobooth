//
//  MenuViewController.swift
//  PhotoBooth
//
//  Created by Руслан on 15.03.2021.
//

import UIKit

final class MenuViewController: UIViewController {
    private var takePhotoButton: UIButton!
    private var viewPhotosButton: UIButton!

    private let viewModel: MenuViewModel

    init(viewModel: MenuViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = UIView()
        view.backgroundColor = .white

        takePhotoButton = makeButton(title: "Take a photo") { [unowned self] in
            self.viewModel.didSelectTakePhoto()
        }

        viewPhotosButton = makeButton(title: "View photos") { [unowned self] in
            self.viewModel.didSelectViewPhotos()
        }

        let stack = UIStackView(arrangedSubviews: [takePhotoButton, viewPhotosButton])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.spacing = 30.0
        view.addSubview(stack)

        NSLayoutConstraint.activate([
            stack.leftAnchor.constraint(equalTo: view.leftAnchor),
            stack.rightAnchor.constraint(equalTo: view.rightAnchor),
            stack.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

private extension MenuViewController {
    func makeButton(title: String, tapAction: @escaping () -> Void) -> UIButton {
        let button = UIButton(type: .system, primaryAction: .init { _ in
            tapAction()
        })
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = .boldSystemFont(ofSize: 30.0)
        button.setTitle(title, for: .normal)

        return button
    }
}
