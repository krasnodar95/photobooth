//
//  MenuViewModel.swift
//  PhotoBooth
//
//  Created by Руслан on 15.03.2021.
//

final class MenuViewModel {
    private let router: RouterProtocol
    private let screenFactory: ScreenFactoryProtocol

    init(router: RouterProtocol, screenFactory: ScreenFactoryProtocol) {
        self.router = router
        self.screenFactory = screenFactory
    }

    func didSelectTakePhoto() {
        router.showPushed(screenFactory.makeTakePhoto(router: router))
    }

    func didSelectViewPhotos() {
        router.showPushed(screenFactory.makePhotoList(router: router))
    }
}
