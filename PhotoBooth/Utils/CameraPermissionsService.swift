//
//  CameraPermissionsService.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit
import AVFoundation

public protocol CameraPermissionsServiceProtocol {
    func checkStatus() -> CameraPermissionsServiceStatus
    func requestGrants(completion: @escaping (Bool) -> Void)
}

public enum CameraPermissionsServiceStatus {
    case needRequestAccess
    case accessGranted
    case accessDenied
    case noTechnicalPossibility
}

final class CameraPermissionsService: CameraPermissionsServiceProtocol {
    func checkStatus() -> CameraPermissionsServiceStatus {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            return .noTechnicalPossibility
        }

        let avStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch avStatus {
        case .authorized:
            return .accessGranted
        case .notDetermined:
            return .needRequestAccess
        default:
            return .accessDenied
        }
    }

    func requestGrants(completion: @escaping (Bool) -> Void) {
        AVCaptureDevice.requestAccess(for: .video) { isGranted in
            DispatchQueue.main.async {
                completion(isGranted)
            }
        }
    }
}
