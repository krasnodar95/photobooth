//
//  Signal.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import Foundation

final class Signal<T> {
    private var observers: [UUID: Subscription<T>] = [:]

    init() {}

    @discardableResult
    func subscribe(_ block: @escaping (T) -> Void) -> Cancellable {
        let uuid = UUID()
        let subscription = Subscription(
            callback: block,
            cancelBlock: { [weak self] in self?.observers[uuid] = nil }
        )
        observers[uuid] = subscription
        return subscription
    }

    @discardableResult
    func subscribe<O: AnyObject>(forward object: O, _ block: @escaping (O, T) -> Void) -> Cancellable {
        return subscribe { [weak object] value in
            guard let object = object else { return }
            block(object, value)
        }
    }

    func send(_ value: T) {
        observers.forEach { $0.value.callback(value) }
    }
}

extension Signal where T == Void {
    func send() {
        send(())
    }
}

private final class Subscription<T>: Cancellable {
    fileprivate let callback: (T) -> Void
    private let cancelBlock: () -> Void

    init(callback: @escaping (T) -> Void, cancelBlock: @escaping () -> Void) {
        self.callback = callback
        self.cancelBlock = cancelBlock
    }

    func cancel() {
        cancelBlock()
    }
}

protocol Cancellable: AnyObject {
    func cancel()
}

final class AnyCancellable: Cancellable {
    private let _cancel: () -> Void

    init(cancel: @escaping () -> Void) {
        _cancel = cancel
    }

    func cancel() {
        _cancel()
    }
}
