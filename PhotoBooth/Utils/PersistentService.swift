//
//  PersistentService.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import CoreData

final class PersistentService {
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "PhotoBooth")
        container.loadPersistentStores { (_, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        }

        return container
    }()

    var mainContext: NSManagedObjectContext {
        container.viewContext
    }

    var backgroundContext: NSManagedObjectContext {
        container.newBackgroundContext()
    }
}
