//
//  UITableViewCell+Model.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

extension UITableViewCell {
    struct Model {
        let uuid: UUID
        let image: UIImage?
        let title: String?
        let subtitle: String?

        init(with model: PhotoModel) {
            uuid = model.uuid
            image = model.image
            title = model.name
            subtitle = model.createdAt.toString()
        }
    }

    func update(with model: Model) {
        imageView?.image = model.image
        textLabel?.text = model.title
        detailTextLabel?.text = model.subtitle
    }
}
