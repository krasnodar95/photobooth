//
//  Router.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

public protocol RouterProtocol {
    func showPushed(_ controller: UIViewController)
    func jumpBack()
    func jumpBackToRoot()
    func showModal(_ controller: UIViewController)
    func dismissModal()
}

final class Router: RouterProtocol {
    private let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func showPushed(_ controller: UIViewController) {
        navigationController.pushViewController(controller, animated: true)
    }

    func jumpBack() {
        navigationController.popViewController(animated: true)
    }

    func jumpBackToRoot() {
        navigationController.popToRootViewController(animated: true)
    }

    func showModal(_ controller: UIViewController) {
        navigationController.visibleViewController?.present(controller, animated: true, completion: nil)
    }

    func dismissModal() {
        navigationController.visibleViewController?.dismiss(animated: true, completion: nil)
    }
}
