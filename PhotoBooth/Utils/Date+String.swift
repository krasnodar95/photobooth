//
//  Date+String.swift
//  PhotoBooth
//
//  Created by Руслан on 17.03.2021.
//

import Foundation

extension Date {
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full

        return formatter.string(from: self)
    }
}
