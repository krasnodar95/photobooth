//
//  Photo+PhotoModel.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

extension Photo {
    func toDomain() -> PhotoModel? {
        guard let imageData = image,
              let image =  UIImage(data: imageData),
              let name = name,
              let createdAt = createdAt,
              let uuid = uuid else {
            return nil
        }

        return .init(uuid: uuid, image: image, name: name, createdAt: createdAt)
    }
}
