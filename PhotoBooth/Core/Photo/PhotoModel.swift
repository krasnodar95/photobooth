//
//  PhotoModel.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit

public struct PhotoModel {
    public let uuid: UUID
    public let image: UIImage
    public let name: String
    public let createdAt: Date

    public init(uuid: UUID, image: UIImage, name: String, createdAt: Date) {
        self.uuid = uuid
        self.image = image
        self.name = name
        self.createdAt = createdAt
    }
}
