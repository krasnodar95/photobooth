//
//  PhotoRepository.swift
//  PhotoBooth
//
//  Created by Руслан on 16.03.2021.
//

import UIKit
import CoreData

public protocol PhotoRepositoryProtocol {
    func savePhoto(image: UIImage, name: String, completion: @escaping (Result<Void, Error>) -> Void)
    func fetchPhotoList(completion: @escaping (Result<[PhotoModel], Error>) -> Void)
    func fetchPhoto(with photoUUID: UUID, completion: @escaping (Result<PhotoModel?, Error>) -> Void)
}

final class PhotoRepository: PhotoRepositoryProtocol {
    private let persistentService: PersistentService

    init(persistentService: PersistentService) {
        self.persistentService = persistentService
    }

    func savePhoto(image: UIImage, name: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let context = persistentService.backgroundContext
        context.perform {
            let entity = Photo.entity()
            let photo = Photo(entity: entity, insertInto: context)
            photo.uuid = .init()
            photo.name = name
            photo.image = image.jpegData(compressionQuality: 0.8)
            photo.createdAt = .init()

            do {
                try context.save()
                DispatchQueue.main.async {
                    completion(.success(()))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }

    func fetchPhotoList(completion: @escaping (Result<[PhotoModel], Error>) -> Void) {
        let fetch = NSFetchRequest<Photo>(entityName: "Photo")
        fetch.sortDescriptors = [.init(key: "createdAt", ascending: true)]

        let context = persistentService.mainContext
        context.perform {
            do {
                let photoManagedObjectList = try context.fetch(fetch)
                let photoList = photoManagedObjectList.compactMap { $0.toDomain() }
                completion(.success(photoList))
            } catch {
                completion(.failure(error))
            }
        }
    }

    func fetchPhoto(with photoUUID: UUID, completion: @escaping (Result<PhotoModel?, Error>) -> Void) {
        let fetch = NSFetchRequest<Photo>(entityName: "Photo")
        fetch.fetchLimit = 1
        fetch.predicate = .init(format: "uuid == %@", photoUUID as CVarArg)

        let context = persistentService.mainContext
        context.perform {
            do {
                let photoManagedObject = try context.fetch(fetch).first
                completion(.success(photoManagedObject?.toDomain()))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
