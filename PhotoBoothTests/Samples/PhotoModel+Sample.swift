//
//  PhotoModel+Sample.swift
//  PhotoBoothTests
//
//  Created by Руслан on 17.03.2021.
//

import PhotoBooth
import UIKit

extension PhotoModel {
    static func sample(
        uuid: UUID = .init(),
        image: UIImage = .init(),
        name: String = "PhotoName",
        createdAt: Date = .init()
    ) -> PhotoModel {
        .init(uuid: uuid, image: image, name: name, createdAt: createdAt)
    }
}
