//
//  PhotoRepositoryMock.swift
//  PhotoBoothTests
//
//  Created by Руслан on 17.03.2021.
//

import PhotoBooth
import UIKit

final class PhotoRepositoryMock: PhotoRepositoryProtocol {
    private(set) var image: UIImage?
    private(set) var name: String?

    func savePhoto(image: UIImage, name: String, completion: @escaping (Result<Void, Error>) -> Void) {
        self.image = image
        self.name = name
        completion(.success(()))
    }

    func fetchPhotoList(completion: @escaping (Result<[PhotoModel], Error>) -> Void) {
        completion(.success([.sample(name: "Photo1"), .sample(name: "Photo2")]))
    }

    private(set) var photoUUID: UUID?

    func fetchPhoto(with photoUUID: UUID, completion: @escaping (Result<PhotoModel?, Error>) -> Void) {
        
        self.photoUUID = photoUUID
        completion(.success(.sample()))
    }
}
