//
//  RouterMock.swift
//  PhotoBoothTests
//
//  Created by Руслан on 17.03.2021.
//

import PhotoBooth
import UIKit

final class RouterMock: RouterProtocol {
    private(set) var isShowPushedInvoked = false
    func showPushed(_ controller: UIViewController) {
        isShowPushedInvoked = true
    }

    private(set) var isJumpBackInvoked = false
    func jumpBack() {
        isJumpBackInvoked = true
    }

    private(set) var isJumpBackToRootInvoked = false
    func jumpBackToRoot() {
        isJumpBackToRootInvoked = true
    }

    private(set) var isShowModalInvoked = false
    func showModal(_ controller: UIViewController) {
        isShowModalInvoked = true
    }

    private(set) var isDismissModalInvoked = false
    func dismissModal() {
        isDismissModalInvoked = true
    }
}
