//
//  CameraPermissionsServiceMock.swift
//  PhotoBoothTests
//
//  Created by Руслан on 17.03.2021.
//

import PhotoBooth

final class CameraPermissionsServiceMock: CameraPermissionsServiceProtocol {
    private let currentStatus: CameraPermissionsServiceStatus

    init(status: CameraPermissionsServiceStatus = .needRequestAccess) {
        currentStatus = status
    }

    private(set) var isCheckStatusInvoked = false
    func checkStatus() -> CameraPermissionsServiceStatus {
        isCheckStatusInvoked = true
        return currentStatus
    }

    func requestGrants(completion: @escaping (Bool) -> Void) {
        completion(true)
    }
}
