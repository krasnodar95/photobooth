//
//  ScreenFactoryMock.swift
//  PhotoBoothTests
//
//  Created by Руслан on 17.03.2021.
//

import PhotoBooth
import UIKit

final class ScreenFactoryMock: ScreenFactoryProtocol {
    private(set) var isMakeMenuInvoked = false
    func makeMenu(router: RouterProtocol) -> UIViewController {
        isMakeMenuInvoked = true
        return .init()
    }

    private(set) var isMakeTakePhotoInvoked = false
    func makeTakePhoto(router: RouterProtocol) -> UIViewController {
        isMakeTakePhotoInvoked = true
        return .init()
    }

    private(set) var currentText: String?
    func makePrompt(currentText: String?, completion: @escaping (String?) -> Void) -> UIViewController {
        self.currentText = currentText
        completion("Some new text")
        return .init()
    }

    private(set) var isMakePhotoListInvoked = false
    func makePhotoList(router: RouterProtocol) -> UIViewController {
        isMakePhotoListInvoked = true
        return .init()
    }

    private(set) var photoUUID: UUID?
    private(set) var isMakePhotoDetailsInvoked = false
    func makePhotoDetails(photoUUID: UUID) -> UIViewController {
        self.photoUUID = photoUUID
        isMakePhotoDetailsInvoked = true
        return .init()
    }
}
