//
//  TakePhotoViewModelTests.swift
//  PhotoBoothTests
//
//  Created by Руслан on 17.03.2021.
//

import XCTest
@testable import PhotoBooth

class TakePhotoViewModelTests: XCTestCase {
    var viewModel: TakePhotoViewModel!
    var router: RouterMock!
    var screenFactory: ScreenFactoryMock!
    var cameraPermissionsService: CameraPermissionsServiceMock!
    var photoRepository: PhotoRepositoryMock!

    override func setUpWithError() throws {
        router = .init()
        screenFactory = .init()
        photoRepository = .init()
    }

    override func tearDownWithError() throws {
        viewModel = nil
        router = nil
        screenFactory = nil
        cameraPermissionsService = nil
        photoRepository = nil
    }

    func testViewDidLoad_needRequestStatus() throws {
        //
        cameraPermissionsService = .init(status: .needRequestAccess)
        viewModel = .init(
            router: router,
            screenFactory: screenFactory,
            cameraPermissionsService: cameraPermissionsService,
            photoRepository: photoRepository
        )

        var isNormalStateEstablished = false
        viewModel.screenStateSignal.subscribe { state in
            switch state {
            case .normal:
                isNormalStateEstablished = true
            default: break
            }
        }

        //
        viewModel.viewDidLoad()

        //
        XCTAssertTrue(isNormalStateEstablished)
    }

    func testViewDidLoad_accessGrantedStatus() throws {
        //
        cameraPermissionsService = .init(status: .accessGranted)
        viewModel = .init(
            router: router,
            screenFactory: screenFactory,
            cameraPermissionsService: cameraPermissionsService,
            photoRepository: photoRepository
        )

        var isNormalStateEstablished = false
        viewModel.screenStateSignal.subscribe { state in
            switch state {
            case .normal:
                isNormalStateEstablished = true
            default: break
            }
        }

        //
        viewModel.viewDidLoad()

        //
        XCTAssertTrue(isNormalStateEstablished)
    }

    func testViewDidLoad_accessDeniedStatus() throws {
        //
        cameraPermissionsService = .init(status: .accessDenied)
        viewModel = .init(
            router: router,
            screenFactory: screenFactory,
            cameraPermissionsService: cameraPermissionsService,
            photoRepository: photoRepository
        )

        let expectedModel: StatusView.Model = .init(title: "Camera permission denied. Please give permissions in system settings ⚙️", image: .init(imageLiteralResourceName: "attention-sign"))

        var isWarningEstablished = false
        var establishedModel: StatusView.Model?
        viewModel.screenStateSignal.subscribe { state in
            switch state {
            case let .warning(model):
                isWarningEstablished = true
                establishedModel = model
            default: break
            }
        }

        //
        viewModel.viewDidLoad()

        //
        XCTAssertTrue(isWarningEstablished)
        XCTAssertEqual(expectedModel, establishedModel)
    }

    func testViewDidLoad_noTechnicalPossibilityStatus() throws {
        //
        cameraPermissionsService = .init(status: .noTechnicalPossibility)
        viewModel = .init(
            router: router,
            screenFactory: screenFactory,
            cameraPermissionsService: cameraPermissionsService,
            photoRepository: photoRepository
        )

        let expectedModel: StatusView.Model = .init(title: "There is no camera on device", image: .init(imageLiteralResourceName: "no-photo"))

        var isWarningEstablished = false
        var establishedModel: StatusView.Model?
        viewModel.screenStateSignal.subscribe { state in
            switch state {
            case let .warning(model):
                isWarningEstablished = true
                establishedModel = model
            default: break
            }
        }

        //
        viewModel.viewDidLoad()

        //
        XCTAssertTrue(isWarningEstablished)
        XCTAssertEqual(expectedModel, establishedModel)
    }

    func testMakePhotoSelected() {
        //
        cameraPermissionsService = .init(status: .noTechnicalPossibility)
        viewModel = .init(
            router: router,
            screenFactory: screenFactory,
            cameraPermissionsService: cameraPermissionsService,
            photoRepository: photoRepository
        )

        var isSignalSended = false
        viewModel.pickImageSignal.subscribe {
            isSignalSended = true
        }

        //
        viewModel.makePhotoSelected()

        //
        XCTAssertTrue(isSignalSended)
    }
}
