//
//  PhotoBoothTests.swift
//  PhotoBoothTests
//
//  Created by Руслан on 15.03.2021.
//

import XCTest
@testable import PhotoBooth

class MenuViewModelTests: XCTestCase {
    var viewModel: MenuViewModel!
    var router: RouterMock!
    var screenFactory: ScreenFactoryMock!

    override func setUpWithError() throws {
        router = .init()
        screenFactory = .init()

        viewModel = .init(
            router: router,
            screenFactory: screenFactory
        )
    }

    override func tearDownWithError() throws {
        viewModel = nil
        router = nil
        screenFactory = nil
    }

    func testDidSelectTakePhoto() throws {
        viewModel.didSelectTakePhoto()

        XCTAssertTrue(screenFactory.isMakeTakePhotoInvoked)
        XCTAssertTrue(router.isShowPushedInvoked)
    }

    func testDidSelectViewPhotos() throws {
        viewModel.didSelectViewPhotos()

        XCTAssertTrue(screenFactory.isMakePhotoListInvoked)
        XCTAssertTrue(router.isShowPushedInvoked)
    }
}
